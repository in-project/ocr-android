package com.inproject.ocrandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScanResultActivity extends AppCompatActivity {


    @BindView(R.id.tv_scan_result)
    TextView tv_scan_result;

    @BindView(R.id.tv_title)
    TextView tv_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_result);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_title.setText("Hasil Scan "+getIntent().getStringExtra("TAG"));
        tv_scan_result.setText(getIntent().getStringExtra("VALUE_TAG"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}